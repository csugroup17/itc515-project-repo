package library.payfine;
import library.entities.Library;
import library.entities.Member;

public class PayFineControl {
	
	private PayFineUi ui;  //Changed Ui->ui & PayFineUi->PayFineUi-Sandesh
	private enum ControlState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };  //Changed CONTROL_STATE->ControlState-Sandesh
	private ControlState STATE;  //Changed sTaTe->STATE-Sandesh
	
	private Library library;   //Changed LiBrArY->library-Sandesh
	private Member member;  //Changed MeMbEr->member-Sandesh


	public PayFineControl() {
		this.library = Library.getInstance();  //Changed GeTiNsTaNcE->getInstance-Sandesh
		STATE = ControlState.INITIALISED;
	}
	
	
	public void setUi(PayFineUi ui) {  //Changed SeT_uI->setUi & uI->ui-Sandesh
		if (!STATE.equals(ControlState.INITIALISED)) {
			throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
		}	
		this.ui = ui;
		ui.setState(PayFineUi.UiState.READY);  //Changed UiState->UiState & setState->setState -Sandesh
		STATE = ControlState.READY;		
	}


	public void cardSwiped(int memberId) {  //Changed cardSwiped->cardSwiped-Sandesh
		if (!STATE.equals(ControlState.READY)) 
			throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
			
		member = library.getMember(memberId);  //Changed getMember->getMember & MeMbEr_Id->memberId-Sandesh
		
		if (member == null) {
			ui.display("Invalid Member Id");  //Changed display->display-Sandesh
			return;
		}
		ui.display(member.toString());
		ui.setState(PayFineUi.UiState.PAYING);
		STATE = ControlState.PAYING;
	}
	
	
	public void cancel() {  //Changed cancel->cancel-Sandesh
		ui.setState(PayFineUi.UiState.CANCELLED);
		STATE = ControlState.CANCELLED;
	}


	public double payFine(double AmOuNt) {  //Changed payFine->payFine-Sandesh
		if (!STATE.equals(ControlState.PAYING)) 
			throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
			
		double change = member.payFine(AmOuNt);  //Changed ChAnGe->change-Sandesh
		if (change > 0) 
			ui.display(String.format("Change: $%.2f", change));
		
		ui.display(member.toString());
		ui.setState(PayFineUi.UiState.COMPLETED);
		STATE = ControlState.COMPLETED;
		return change;
	}
	


}
