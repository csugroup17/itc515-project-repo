package library.payfine;
import java.util.Scanner;


public class PayFineUi {


	public static enum UiState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };

	private PayFineControl control;  //Changed CoNtRoL->control-Sandesh
	private Scanner input;
	private UiState STATE;  //Changed StAtE->STATE-Sandesh

	
	public PayFineUi(PayFineControl control) {
		this.control = control;
		input = new Scanner(System.in);
		STATE = UiState.INITIALISED;
		control.setUi(this);
	}
	
	
	public void setState(UiState state) {
		this.STATE = state;
	}


	public void run() {  //Changed run->run-Sandesh
		output("Pay Fine Use Case UI\n");
		
		while (true) {
			
			switch (STATE) {
			
			case READY:
				String memStr = input("Swipe member card (press <enter> to cancel): ");  //Changed Mem_Str->memStr-Sandesh
				if (memStr.length() == 0) {
					control.cancel();
					break;
				}
				try {
					int memberId = Integer.valueOf(memStr).intValue();  //Changed Member_ID->memberId-Sandesh
					control.cardSwiped(memberId);
				}
				catch (NumberFormatException e) {
					output("Invalid memberId");
				}
				break;
				
			case PAYING:
				double amount = 0;   //Changed AmouNT->amount-Sandesh
				String amtStr = input("Enter amount (<Enter> cancels) : ");  //Changed Amt_Str->amtStr-Sandesh
				if (amtStr.length() == 0) {
					control.cancel();
					break;
				}
				try {
					amount = Double.valueOf(amtStr).doubleValue();
				}
				catch (NumberFormatException e) {}
				if (amount <= 0) {
					output("Amount must be positive");
					break;
				}
				control.payFine(amount);
				break;
								
			case CANCELLED:
				output("Pay Fine process cancelled");
				return;
			
			case COMPLETED:
				output("Pay Fine process complete");
				return;
			
			default:
				output("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + STATE);			
			
			}		
		}		
	}

	
	private String input(String prompt) {
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void output(Object object) {
		System.out.println(object);
	}	
			

	public void display(Object object) {
		output(object);
	}


}
