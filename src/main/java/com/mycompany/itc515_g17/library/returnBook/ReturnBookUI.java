package library.returnBook;
import java.util.Scanner;


public class ReturnBookUI {

	public static enum UiState { INITIALISED, READY, INSPECTING, COMPLETED };

	private ReturnBookControl control; /*CoNtRoL refactored to control*/
	private Scanner input; /*iNpUt refactored to input*/
	private UiState state; /*sTaTe refactored to state*/

	
	public ReturnBookUI(ReturnBookControl control) { /*CoNtRoL refactored to control*/
		this.control = control;
		input = new Scanner(System.in);
		state = UiState.INITIALISED;
		control.setUi(this);
	}


	public void run() {	/*RuN refactored to run*/	
		output("Return Book Use Case UI\n"); /*oUtPuT refactored to output*/	
		
		while (true) {
			
			switch (state) {
			
			case INITIALISED:
				break;
				
			case READY:
				String bookInputString = input("Scan Book (<enter> completes): "); /*BoOk_InPuT_StRiNg refactored to bookInputString*/ /*iNpUt refactored to input*/
				if (bookInputString.length() == 0) 
					control.scanningComplete();
				
				else {
					try {
						int bookId = Integer.valueOf(bookInputString).intValue(); /*Book_Id refactored to bookId*/	
						control.bookScanned(bookId);
					}
					catch (NumberFormatException e) {
						output("Invalid bookId");
					}					
				}
				break;				
				
				
			case INSPECTING:
				String ans = input("Is book damaged? (Y/N): "); /*AnS refactored to ans*/	
				boolean isDamaged = false; /*Is_DAmAgEd refactored to isDamaged*/
				if (ans.toUpperCase().equals("Y")) 					
					isDamaged = true;
				
				control.dischargeLoan(isDamaged);


			
			case COMPLETED:
				output("Return processing complete");
				return;
			
			default:
				output("Unhandled state");
				throw new RuntimeException("ReturnBookUI : unhandled state :" + state);			
			}
		}
	}

	
	private String input(String prompt) { /*PrOmPt refactored to prompt*/
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void output(Object object) { /*ObJeCt refactored to object*/
		System.out.println(object);
	}
	
			
	public void display(Object object) {
		output(object);
	}
	
	public void setState(UiState state) {
		this.state = state;
	}

	
}
