package library.returnBook;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class ReturnBookControl { /*rETURN_bOOK_cONTROL refactored to ReturnBookControl*/

	private ReturnBookUI ui; /*Ui refactored to ui*/
	private enum ControlState { INITIALISED, READY, INSPECTING }; /*cOnTrOl_sTaTe refactored to ControlState*//*cOnTrOl_sTaTe refactored to ControlState*/
	private ControlState state; /*sTaTe refactored to state*/
	
	private Library library; /*lIbRaRy refactored to library*/
	private Loan currentLoan; /*CurrENT_loan refactored to currentLoan*/
	

	public ReturnBookControl() {
		this.library = Library.getInstance(); /*refactored to getInstance */
		state = ControlState.INITIALISED;
	}
	
	
	public void setUi(ReturnBookUI ui) { /*sEt_uI refactored to setUi */ /*uI refactored to ui*/
		if (!state.equals(ControlState.INITIALISED)) 
			throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");
		
		this.ui = ui;
		ui.setState(ReturnBookUI.UiState.READY); /*sEt_sTaTe refactored to setState */
		state = ControlState.READY;		/*uI_sTaTe refactored to UiState */
	}


	public void bookScanned(int bookId) { /*bOoK_sCaNnEd refactored to bookScanned */ /*bOoK_iD refactored to bookId */
		if (!state.equals(ControlState.READY)) 
			throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
		
		Book currentBook = library.getBook(bookId); /*cUrReNt_bOoK refactored to currentBook */ 
                                                                /*gEt_BoOk  refactored to getBook */ 
		if (currentBook == null) {
			ui.display("Invalid Book Id"); /*DiSpLaY refactored to display */ 
			return;
		}
		if (!currentBook.isOnLoan()) {
			ui.display("Book has not been borrowed");
			return;
		}		
		currentLoan = library.getLoanByBookId(bookId);  /*GeT_LoAn_By_BoOkId refactored to getLoanByBookId */ 	
		double overDueFine = 0.0;  /*Over_Due_Fine refactored to overDueFine */ 
		if (currentLoan.isOverDue()) 
			overDueFine = library.calculateOverDueFine(currentLoan); /*CaLcUlAtE_OvEr_DuE_FiNe refactored to calculateOverDueFine */ 
		
		ui.display("Inspecting");
		ui.display(currentBook.toString());
		ui.display(currentLoan.toString());
		
		if (currentLoan.isOverDue()) 
			ui.display(String.format("\nOverdue fine : $%.2f", overDueFine));
		
		ui.setState(ReturnBookUI.UiState.INSPECTING);
		state = ControlState.INSPECTING;		
	}


	public void scanningComplete() {  /*sCaNnInG_cOmPlEtE refactored to scanningComplete */ 
		if (!state.equals(ControlState.READY)) 
			throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");
			
		ui.setState(ReturnBookUI.UiState.COMPLETED);		
	}


	public void dischargeLoan(boolean isDamaged) { /*dIsChArGe_lOaN refactored to dischargeLoan */ 
		if (!state.equals(ControlState.INSPECTING)) /*iS_dAmAgEd refactored to isDamaged */ 
			throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
		
		library.dischargeLoan(currentLoan, isDamaged);/*DiScHaRgE_LoAn refactored to dischargeLoan */ 
		currentLoan = null;
		ui.setState(ReturnBookUI.UiState.READY);
		state = ControlState.READY;				
	}


}
