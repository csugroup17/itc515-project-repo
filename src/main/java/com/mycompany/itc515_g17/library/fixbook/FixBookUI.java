package library.fixbook;
import java.util.Scanner;


public class  FixBookUi {    
  
	public static enum UiState { INITIALISED, READY, FIXING, COMPLETED };  //Changed uiState->UiState-Sandesh  

	private FixBookControl control;  //Changed CoNtRoL->control-Sandesh
	private Scanner input;  //Changed InPuT->input-Sandesh
	private UiState state;  //Changed StAtE->state-Sandesh

	
	public FixBookUi(FixBookControl control) {  //Changed CoNtRoL->control-Sandesh
		this.control = control;
		input = new Scanner(System.in);
		state = UiState.INITIALISED;
		control.setUi(this);
	}


	public void setState(UiState state) {
		this.state = state;
	}

	
	public void run() {  //Changed RuN->run-Sandesh
		output("Fix Book Use Case UI\n");
		
		while (true) {
			
			switch (state) {
			
			case READY:
				String bookEntryString = input("Scan Book (<enter> completes): ");  //Changed BoOk_EnTrY_StRiNg->bookEntryString & iNpUt->input -Sandesh
				if (bookEntryString.length() == 0) 
					control.scanningComplete();
				
				else {
					try {
						int bookId = Integer.valueOf(bookEntryString).intValue();  //Changed BoOk_Id->bookId-Sandesh
						control.bookScanned(bookId);
					}
					catch (NumberFormatException e) {
						output("Invalid bookId");  //Changed output->output-Sandesh
					}
				}
				break;	
				
			case FIXING:
				String ans = input("Fix Book? (Y/N) : ");  //Changed AnS->ans-Sandesh
				boolean fix = false;  //Changed FiX->fix-Sandesh
				if (ans.toUpperCase().equals("Y")) 
					fix = true;
				
				control.fixBook(fix);
				break;
								
			case COMPLETED:
				output("Fixing process complete");
				return;
			
			default:
				output("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + state);			
			
			}		
		}
		
	}

	
	private String input(String prompt) {
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void output(Object object) {
		System.out.println(object);
	}
	

	public void display(Object object) {
		output(object);
	}
	
	
}
