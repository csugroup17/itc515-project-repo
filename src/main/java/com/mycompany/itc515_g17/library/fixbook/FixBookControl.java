package library.fixbook;
import library.entities.Book;
import library.entities.Library;

public class FixBookControl {  //Changed FixBookControl->FixBookControl-Sandesh
	
	private FixBookUi ui;  //Changed Ui->ui-Sandesh
	private enum ControlState { INITIALISED, READY, FIXING };
	private ControlState STATE;  //Changed CoNtRoL_StAtE->ControlState & StAtE->STATE-Sandesh
	
	private Library library;  //Changed LiBrArY->library-Sandesh
	private Book currentBook;  //Changed CuRrEnT_BoOk->currentBook-Sandesh


	public FixBookControl() {
		this.library = Library.getInstance();
		STATE = ControlState.INITIALISED;
	}
	
	
	public void setUi(FixBookUi ui) {  //Changed SeT_Ui->setUi-Sandesh
		if (!STATE.equals(ControlState.INITIALISED)) 
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
			
		this.ui = ui;
		ui.setState(FixBookUi.UiState.READY);  //Changed setState->setState & UiState->UiState-Sandesh
		STATE = ControlState.READY;		
	}


	public void bookScanned(int bookId) {  //Changed bookScanned->bookScanned & BoOkId->bookId-Sandesh
		if (!STATE.equals(ControlState.READY)) 
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
			
		currentBook = library.getBook(bookId);  //Changed getBook->getBook-Sandesh
		
		if (currentBook == null) {
			ui.display("Invalid bookId");  //Changed display->display-Sandesh
			return;
		}
		if (!currentBook.isDamaged()) {
			ui.display("Book has not been damaged");
			return;
		}
		ui.display(currentBook.toString());
		ui.setState(FixBookUi.UiState.FIXING);
		STATE = ControlState.FIXING;		
	}


	public void fixBook(boolean mustFix) {  //Changed fixBook->fixBook & mUsT_FiX->mustFix-Sandesh
		if (!STATE.equals(ControlState.FIXING)) 
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
			
		if (mustFix) 
			library.repairBook(currentBook);  //Changed repairBook->repairBook-Sandesh
		
		currentBook = null;
		ui.setState(FixBookUi.UiState.READY);
		STATE = ControlState.READY;		
	}

	
	public void scanningComplete() {  //Changed scanningComplete->scanningComplete-Sandesh
		if (!STATE.equals(ControlState.READY)) 
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
			
		ui.setState(FixBookUi.UiState.COMPLETED);		
	}

}
