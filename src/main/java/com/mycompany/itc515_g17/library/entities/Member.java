package library.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Member implements Serializable {

    private String lastName;    //changed LaSt_NaMe to lastName-Sandesh 
    private String firstName;  //changed FiRsT_NaMe to firstName-Sandesh 
    private String emailAddress;   //changed EmAiL_AdDrEsS to emailAddress-Sandesh 
    private int phoneNumber;       //changed PhOnE_NuMbEr to phoneNumber-Sandesh 
    private int memberId;    //changed memberId to memberId-Sandesh
    private double finesOwing;   //changed FiNeS_OwInG to finesOwing-Sandesh

    private Map<Integer, Loan> currentLoans;  //changed cUrReNt_lOaNs to currentLoans-Sandesh

    public Member(String lastName, String firstName, String emailAddress, int phoneNumber, int memberId) {
        this.lastName = lastName;     //changed LaSt_NaMe to lastName-Sandesh 
        this.firstName = firstName;    //changed FiRsT_NaMe to firstName-Sandesh 
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.memberId = memberId;

        this.currentLoans = new HashMap<>();//changed cUrReNt_lOaNs to currentLoans-Sandesh
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Member:  ").append(memberId).append("\n")
                .append("  Name:  ").append(lastName).append(", ").append(firstName).append("\n") //changed LaSt_NaMe to lastName and FiRsT_NaMe to firstName -Sandesh 
                .append("  Email: ").append(emailAddress).append("\n") //changed EmAiL_AdDrEsS to emailAddress-Sandesh
                .append("  Phone: ").append(phoneNumber)
                .append("\n")
                .append(String.format("  Fines Owed :  $%.2f", finesOwing)) //changed FiNeS_OwInG to finesOwing-Sandesh
                .append("\n");

        for (Loan loan : currentLoans.values()) {  //changed cUrReNt_lOaNs to currentLoans and loAn to loan-Sandesh
            sb.append(loan).append("\n");  //changed loAn to loan-Sandesh
        }
        return sb.toString();
    }

    public int getId() {   //changed GeT_ID to getId-Sandesh
        return memberId;
    }

    public List<Loan> getLoans() {  //changed GeT_LoAnS to getLoans
        return new ArrayList<Loan>(currentLoans.values());  //changed cUrReNt_lOaNs to currentLoans-Sandesh
    }

    public int getNumberOfCurrentLoans() {      //changed gEt_nUmBeR_Of_CuRrEnT_LoAnS() to getNumberOfCurrentLoans()-Sandesh
        return currentLoans.size();  //changed cUrReNt_lOaNs to currentLoans-Sandesh
    }

    public double finesOwed() {  //changed FiNeS_OwEd to finesOwed-Sandesh
        return finesOwing;  //changed FiNeS_OwInG to finesOwing-Sandesh
    }

    public void takeOutLoan(Loan loan) {  //changed TaKe_OuT_LoAn to takeOutLoan-Sandesh
        if (!currentLoans.containsKey(loan.getId())) //changed cUrReNt_lOaNs to currentLoans-Sandesh
        {
            currentLoans.put(loan.getId(), loan);  //changed cUrReNt_lOaNs to currentLoans-Sandesh
        } else {
              throw new RuntimeException("Duplicate loan added to member");
        }

    }

    public String getLastName() {  //changed Get_Last_Name to getLastName-Sandesh
        return firstName;
    }

    public String getFirstName() {  //changed GeT_FiRsT_NaMe() to getFirstName-Sandesh
        return firstName;
    }

    public void addFine(double fine) {  //changed AdDFine to addFine-Sandesh
        finesOwing += fine;  //changed FiNeS_OwInG to finesOwing-Sandesh
    }

    public double payFine(double amount) {  //changed PaY_FiNe to payFine and AmOuNt to amount-Sandesh
        if (amount < 0) //changed AmOuNt to amount-Sandesh
        {
            throw new RuntimeException("Member.payFine: amount must be positive");
        }

        double change = 0;
        if (amount > finesOwing) {  //changed FiNeS_OwInG to finesOwing and AmOuNt to amount-Sandesh
            change = amount - finesOwing;  //changed FiNeS_OwInG to finesOwing and AmOuNt to amount-Sandesh
            finesOwing = 0;  //changed FiNeS_OwInG to finesOwing-Sandesh
        } 
        else {
            finesOwing -= amount;  //changed FiNeS_OwInG to finesOwing and AmOuNt to amount-Sandesh
        }
        return change;
    }

    public void dischargeLoan(Loan loan) {  //changed Discharge_Loan to dischargeLoan and LoAn to loan-Sandesh
        if (currentLoans.containsKey(loan.getId())){ //changed cUrReNt_lOaNs to currentLoans-Sandesh
            currentLoans.remove(loan.getId());  //changed cUrReNt_lOaNs to currentLoans-Sandesh
        } else {
            throw new RuntimeException("No such loan held by member");
        }
    }
}