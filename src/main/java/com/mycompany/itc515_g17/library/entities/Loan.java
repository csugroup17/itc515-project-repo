package library.entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {

    /*loanId renamed to Loan */

    public static enum LoanState {
        CURRENT, OVER_DUE, DISCHARGED
    };
    /*lOaN_sTaTe renamed to LoanState*//*lOaN_sTaTe renamed to LoanState*/

    private int loanId;
    /*LoAn_Id renamed to loanId*/
    private Book book;
    /*BoOk renamed to book*/
    private Member member;
    /*MeMbEr renamed to member*/
    private Date date;
    /*DaTe renamed to date*/
    private LoanState state;

    /*StAtE renamed to state*/


    public Loan(int loanId, Book book, Member member, Date dueDate) {
        /*BoOk renamed to book*/
        this.loanId = loanId;
        /**/
        this.book = book;
        this.member = member;
        /*MemMber  renamed to member*/
        this.date = dueDate;
        /*DuE_dAtE  renamed to dueDate*/
        this.state = LoanState.CURRENT;
    }

    public void checkOverDue() {
        /*cHeCk_OvEr_DuE  renamed to checkOverDue*/
        if (state == LoanState.CURRENT
                && Calendar.getInstance().getDate().after(date)) {  /*gEtInStAnCe  renamed to getInstance & gEt_DaTe to getDate*/
            this.state = LoanState.OVER_DUE; 
        }

    }

    public boolean isOverDue() {
        /*Is_OvEr_DuE  renamed to isOverDue*/
        return state == LoanState.OVER_DUE;
    }

    public Integer getId() {
        /*GeT_Id  renamed to getId*/
        return loanId;
    }

    public Date getDueDate() {
        /*GeT_DuE_DaTe  renamed to getDueDate*/
        return date;
    }

    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        StringBuilder sb = new StringBuilder();
        sb.append("Loan:  ").append(loanId).append("\n")
                .append("  Borrower ").append(member.getId()).append(" : ")  /*GeT_Id  renamed to getId*/
                .append(member.getLastName()).append(", ").append(member.getFirstName()).append("\n") /*GeT_LaSt_NaMe and GeT_FiRsT_NaMe  renamed to getLastName & getFirstName respectively*/
                .append("  Book ").append(book.getId()).append(" : ")
                .append(book.getTitle()).append("\n")
                .append("  DueDate: ").append(sdf.format(date)).append("\n")
                .append("  State: ").append(state);
        return sb.toString();
    }

    public Member getMember() {
        /*GeT_MeMbEr  renamed to getMember*/
        return member;
    }

    public Book getBook() {
        /*GeT_BoOk  renamed to getBook*/
        return book;
    }

    public void discharge() {
        /*DiScHaRgE  renamed to discharge*/
        state = LoanState.DISCHARGED;
    }

}
