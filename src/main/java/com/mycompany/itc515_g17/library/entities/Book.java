package library.entities;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Book implements Serializable {

    private String title;
    /*the variable  name changed*/
    private String author;
    /*the variable Author  name changed to author*/
    private String callNo;
    /*the variable CAllNo  name changed to callNo*/
    private int id;

    /*the variable iD  changed to id*/

    private enum State {
        AVAILABLE, ON_LOAN, DAMAGED, RESERVED
    };
    /*enum sTaTe   changed to State*//*enum sTaTe   changed to State*//*enum sTaTe   changed to State*//*enum sTaTe   changed to State*/
    private State state;

    /*State changed to state*/


    public Book(String author, String title, String callNo, int id) {
        this.author = author;
        this.title = title;
        this.callNo = callNo;
        this.id = id;
        this.state = State.AVAILABLE;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Book: ").append(id).append("\n")
                .append("  Title:  ").append(title).append("\n")
                .append("  Author: ").append(author).append("\n")
                .append("  CallNo: ").append(callNo).append("\n")
                .append("  State:  ").append(state);

        return sb.toString();
    }

    public Integer getId() {
        /*gEtId changed to getId*/
        return id;
    }

    public String getTitle() {
        /*gEtTiTle changed to getTitle*/
        return title;
    }

    public boolean isAvailable() {
        /* is_AvAiLaBLE renamed to isAvailable*/
        return state == State.AVAILABLE;
    }

    public boolean isOnLoan() {
        /* is_On_LoAn renamed to isOnLoan*/
        return state == State.ON_LOAN;
    }

    public boolean isDamaged() {
        /* is_DaMaGeD renamed to isDamaged*/
        return state == State.DAMAGED;
    }

    public void borrow() {
        /* BoRrOw renamed to borrow*/
        if (state.equals(State.AVAILABLE)) {
            state = State.ON_LOAN;
        } else {
            throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", state));
        }

    }

    public void ReTuRn(boolean damaged) {
        /* DaMaGeD renamed to damaged*/
        if (state.equals(State.ON_LOAN)) {
            if (damaged) {
                state = State.DAMAGED;
            } else {
                state = State.AVAILABLE;
            }
        } else {
            throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", state));
        }

    }

    public void repair() {
        /* RePaIr renamed to repair*/
        if (state.equals(State.DAMAGED)) {
            state = State.AVAILABLE;
        } else {
            throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", state));
        }

    }

}
