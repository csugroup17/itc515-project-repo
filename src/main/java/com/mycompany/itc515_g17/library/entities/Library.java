package library.entities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Library implements Serializable {

	
	private static final long serialVersionUID = 6542641061597479613L; //added new constant variable to solve code not running issue
	private static final String LIBRARY_FILE = "library.obj"; //changed lIbRaRyFiLe to LIBRARY_FILE
	private static final int LOAN_LIMIT = 2; //changed lOaNlImIt to LOAN_LIMIT
	private static final int LOAN_PERIOD = 2; //changed loanPeriod to LOAN_PERIOD
	private static final double FINE_PER_DAY = 1.0; //changed FiNe_PeR_DaY to FINE_PER_DAY
	private static final double MAX_FINES_OWED = 1.0; //changed maxFinesOwed to MAX_FINES_OWED
	private static final double DAMAGE_FEE = 2.0; //changed damageFee to DAMAGE_FEE
	
	private static Library self; //changed SeLf to self
	private int bookId; //changed bOoK_Id to bookId
	private int memberId; //changed mEmBeR_Id to memberId
	private int loanId; //changed lOaN_Id to loanId
	private Date loanDate; //cannot change deu to error java.lang.NullPointerException
	
	private Map<Integer, Book> CaTaLoG; //cannot change deu to error java.lang.NullPointerException
	private Map<Integer, Member> MeMbErS; //cannot change deu to error java.lang.NullPointerException
	private Map<Integer, Loan> loans; //changed lOaNs to loans
	private Map<Integer, Loan> currentLoans; //changed CuRrEnT_LoAnS to currentLoans
	private Map<Integer, Book> damagedBooks; //changed DaMaGeD_BoOkS to damagedBooks
	

	private Library() {
		CaTaLoG = new HashMap<>();
		MeMbErS = new HashMap<>();
		loans = new HashMap<>();
		currentLoans = new HashMap<>();
		damagedBooks = new HashMap<>();
		bookId = 1;
		memberId = 1;		
		loanId = 1;		
	}

	
	public static synchronized Library getInstance() {	//changed method GeTiNsTaNcE to getInstance	
		if (self == null) {
			Path PATH = Paths.get(LIBRARY_FILE);			
			if (Files.exists(PATH)) {	
				try (ObjectInputStream libraryFile = new ObjectInputStream(new FileInputStream(LIBRARY_FILE));) { //changed object name LiBrArY_FiLe to libraryFile
			    
					self = (Library) libraryFile.readObject(); //changed LiBrArY_FiLe to libraryFile
					Calendar.getInstance().setDate(self.loanDate);
					libraryFile.close(); //changed LiBrArY_FiLe to libraryFile
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			else self = new Library();
		}
		return self;
	}

	


	public static synchronized void save() { //changed method SaVe to save
		if (self != null) {
			self.loanDate = Calendar.getInstance().getDate(); //changed method  gEtInStAnCe and gEt_DaTe to getInstance , getDate
			try (ObjectOutputStream libraryFile = new ObjectOutputStream(new FileOutputStream(LIBRARY_FILE));) { //changed object name LiBrArY_fIlE to libraryFile 
				libraryFile.writeObject(self);
				libraryFile.flush();
				libraryFile.close();	

			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	
	public int getBookId() { //changed method name gEt_BoOkId to getBookId
		return bookId;
	}
	
	
	public int getMemberId() { //changed method name gEt_MeMbEr_Id to getMemberId
		return memberId;
	}
	
	
	private int getNextBookId() { //changed method name gEt_NeXt_BoOk_Id to getNextBookId
		return bookId++;
	}

	
	private int getNextMemberId() { //changed method name gEt_NeXt_MeMbEr_Id to getNextMemberId
		return memberId++;
	}

	
	private int getNextLoanId() { //changed method name gEt_NeXt_LoAn_Id to getNextLoanId
		return loanId++;
	}

	
	

	public List<Member> listMembers() {	//changed method name lIsT_MeMbErS to listMembers	

		return new ArrayList<Member>(MeMbErS.values()); 
	}


	

	public List<Book> listBooks() { //changed method name lIsT_BoOkS to listBooks		

		return new ArrayList<Book>(CaTaLoG.values()); 
	}


	public List<Loan> listCurrentLoans() { //changed method name lISt_CuRrEnT_LoAnS to listCurrentLoans
		return new ArrayList<Loan>(currentLoans.values());
	}


	public Member addMember(String lastName, String firstName, String email, int phoneNo) { //changed method name aDd_MeMbEr to addMember
		Member member = new Member(lastName, firstName, email, phoneNo, getNextMemberId());

		MeMbErS.put(member.getId(), member);		
		return member;
	}

	

	public Book addBook(String a, String t, String c) { //changed method name aDd_BoOk to addBook		
		Book b = new Book(a, t, c, getNextBookId());

		CaTaLoG.put(b.getId(), b);		
		return b;
	}

	
	public Member getMember(int memberId) { //changed method name gEt_MeMbEr to getMember
		if (MeMbErS.containsKey(memberId)) 
			return MeMbErS.get(memberId);
		return null;
	}

	
	public Book getBook(int bookId) { //changed method name gEt_BoOk to getBook
		if (CaTaLoG.containsKey(bookId)) 
			return CaTaLoG.get(bookId);		
		return null;
	}

	
	public int getLoanLimit() { //changed method name gEt_LoAn_LiMiT to getLoanLimit
		return LOAN_LIMIT;
	}

	
	public boolean canMemberBorrow(Member member) { //changed method name cAn_MeMbEr_BoRrOw to canMemberBorrow	
		if (member.getNumberOfCurrentLoans() == LOAN_LIMIT ) //changed method name gEt_nUmBeR_Of_CuRrEnT_LoAnS to getNumberOfCurrentLoans
			return false;
				
		if (member.finesOwed()>= MAX_FINES_OWED) //changed method name FiNeS_OwEd to FinesOwed
			return false;
				
		for (Loan loan : member.getLoans()) //changed method name GeT_LoAnS to getLoans
			if (loan.isOverDue()) //changed method name Is_OvEr_DuE to isOverDue
				return false;
			
		return true;
	}

	
	public int getNumberOfLoansRemainingForMember(Member MeMbEr) { //changed method name gEt_NuMbEr_Of_LoAnS_ReMaInInG_FoR_MeMbEr to getNumberOfLoansRemainingForMember
		return LOAN_LIMIT - MeMbEr.getNumberOfCurrentLoans();
	}

	
	public Loan issueLoan(Book book, Member member) { //changed method name iSsUe_LoAn to issueLoan
		Date dueDate = Calendar.getInstance().getDueDate(LOAN_PERIOD); //changed method name gEt_DuE_DaTe to getDueDate
		Loan loan = new Loan(getNextLoanId(), book, member, dueDate);
		member.takeOutLoan(loan); //changed method name TaKe_OuT_LoAn to takeOutLoan
		book.borrow(); //changed method name BoRrOw to borrow
		loans.put(loan.getId(), loan); //changed method name GeT_Id to getId
		currentLoans.put(book.getId(), loan); //changed method name gEtId to getId
		return loan;
	}
	
	
	public Loan getLoanByBookId(int bookId) { //changed method name GeT_LoAn_By_BoOkId to getLoanByBookId
		if (currentLoans.containsKey(bookId)) 
			return currentLoans.get(bookId);
		
		return null;
	}

	
	public double calculateOverDueFine(Loan LoAn) { //changed method name CaLcUlAtE_OvEr_DuE_FiNe to calculateOverDueFine
		if (LoAn.isOverDue()) {
			long daysOverDue = Calendar.getInstance().getDaysDifference(LoAn.getDueDate()); //changed DaYs_OvEr_DuE to daysOverDue also changed method names GeT_DaYs_DiFfErEnCe ,GeT_DuE_DaTe to  getDaysDifference,getDueDate
			double fInE = daysOverDue * FINE_PER_DAY;
			return fInE;
		}
		return 0.0;		
	}


	public void dischargeLoan(Loan currentLoan, boolean isDamaged) { //changed method name DiScHaRgE_LoAn to dischargeLoan and changed cUrReNt_LoAn to currentLoan , iS_dAmAgEd to isDamaged
		Member member = currentLoan.getMember();// changed mEmBeR to member and method name GeT_MeMbEr to getMenber
		Book book  = currentLoan.getBook(); // changed bOoK to member and method name GeT_BoOk to getMenber
		
		double overDueFine = calculateOverDueFine(currentLoan); //changed oVeR_DuE_FiNe to overDueFine
		member.addFine(overDueFine);	//changed method name AdD_FiNe to addFine
		
		member.dischargeLoan(currentLoan);//changed method name dIsChArGeLoAn to dischargeLoan
		book.ReTuRn(isDamaged); //changed method name ReTuRn to Return
		if (isDamaged) {
			member.addFine(DAMAGE_FEE);
			damagedBooks.put(book.getId(), book);
		}
		currentLoan.discharge();//changed method name DiScHaRgE to discharge
		currentLoans.remove(book.getId());
	}


	public void checkCurrentLoans() { //changed method name cHeCk_CuRrEnT_LoAnS to checkCurrentLoans
		for (Loan loan : currentLoans.values()) //changed lOaN to loan and CuRrEnT_LoAnS to currentLoans
			loan.checkOverDue(); //changed method name cHeCk_OvEr_DuE to checkOverDue

				
	}


	public void repairBook(Book currentBook) { //changed method name RePaIr_BoOk to repairBook also changed cUrReNt_BoOk to currentBook
		if (damagedBooks.containsKey(currentBook.getId())) {
			currentBook.repair();//changed method name RePaIr to repair
			damagedBooks.remove(currentBook.getId());
		}
		else 
			throw new RuntimeException("Library: repairBook: book is not damaged");
		
		
	}
	
	
}
