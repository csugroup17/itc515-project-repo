package library.entities;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Calendar {

    private static Calendar self;    //changed sELF to self-Sandesh
    private static java.util.Calendar calendar;    //changed cAlEnDaR to calendar-Sandesh

    private Calendar() {
        calendar = java.util.Calendar.getInstance();    //changed cAlEnDaR to calendar-Sandesh
    }

    public static Calendar getInstance() {     //changed gEtInStAnCe to getInstance-Sandesh
        if (self == null) {    //changed sELF to self-Sandesh
            self = new Calendar();    //changed sELF to self-Sandesh
        }
        return self;    //changed sELF to self-Sandesh
    }

    public void incrementDate(int days) {
        calendar.add(java.util.Calendar.DATE, days);    //changed cAlEnDaR to calendar-Sandesh
    }

    public synchronized void setDate(Date date) {    //changed SeT_DaTe to setDate and DaTe to date-Sandesh
        try {
            calendar.setTime(date);    //changed cAlEnDaR to calendar-Sandesh
            calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);    //changed cAlEnDaR to calendar-Sandesh
            calendar.set(java.util.Calendar.MINUTE, 0);   //changed cAlEnDaR to calendar-Sandesh 
            calendar.set(java.util.Calendar.SECOND, 0); //changed cAlEnDaR to calendar-Sandesh 
            calendar.set(java.util.Calendar.MILLISECOND, 0);//changed cAlEnDaR to calendar-Sandesh
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized Date getDate() {    //changed get_DaTe() to getDate()-Sandesh
        try {
            calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);  //changed cAlEnDaR to calendar-Sandesh
            calendar.set(java.util.Calendar.MINUTE, 0);  //changed cAlEnDaR to calendar-Sandesh
            calendar.set(java.util.Calendar.SECOND, 0);  //changed cAlEnDaR to calendar-Sandesh
            calendar.set(java.util.Calendar.MILLISECOND, 0);//changed cAlEnDaR to calendar-Sandesh
            return calendar.getTime();  //changed cAlEnDaR to calendar-Sandesh
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized Date getDueDate(int loanPeriod) {  //changed gEt_DuE_DaTe to getDueDate()
        Date now = getDate();    //changed get_DaTe() to getDate()-Sandesh
        calendar.add(java.util.Calendar.DATE, loanPeriod);//changed cAlEnDaR to calendar-Sandesh
        Date dueDate = calendar.getTime();//changed cAlEnDaR to calendar and dUeDaTe to dueDate-Sandesh
        calendar.setTime(now);//changed cAlEnDaR to calendar-Sandesh
        return dueDate;
    }

    public synchronized long getDaysDifference(Date targetDate) {    //changed GeT_DaYs_DiFfErEnCe to getDaysDifference-Sandesh

        long diffMillis = getDate().getTime() - targetDate.getTime();   //changed get_DaTe() to getDate() and Diff_Millis to diffMillis-Sandesh
        long diffDays = TimeUnit.DAYS.convert(diffMillis, TimeUnit.MILLISECONDS);          //changed Diff_Millis to diffMillis and Diff_Days to diffDays-Sandesh
        return diffDays;
    }

}
