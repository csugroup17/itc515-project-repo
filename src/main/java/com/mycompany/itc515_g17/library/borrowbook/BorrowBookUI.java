package library.borrowbook;
import java.util.Scanner;


public class BorrowBookUI {
	public static enum UiState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };

	private BorrowBookControl control; //changed CoNtRoL to control
	private Scanner input; //changed InPuT to input
	private UiState state; //changed StaTe to state

	
	public BorrowBookUI(BorrowBookControl control) {
		this.control = control;
		input = new Scanner(System.in);
		state = UiState.INITIALISED;
		control.isSetUi(this);
	}

	
	private String input(String prompt) { //changed method name iNpUT to input and variable name PrOmPt to prompt
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void output(Object object) { //changed method name OuTpUt to output and variable name ObJeCt to object
		System.out.println(object);
	}
	
			
	public void setState(UiState state) {// changed variable name StAtE to state
		this.state = state; //changed StaTe to state
	}

	
	public void run() { //changed method name RuN to run
		output("Borrow Book Use Case UI\n");
		
		while (true) {
			
			switch (state) {			
			
			case CANCELLED:
				output("Borrowing Cancelled");
				return;

				
			case READY:
				String memStr = input("Swipe member card (press <enter> to cancel): "); //changed variable name MEM_STR to memStr
				if (memStr.length() == 0) {
					control.isCancel();
					break;
				}
				try {
					int memberId = Integer.valueOf(memStr).intValue(); //changed variable name MeMbEr_Id to memberId
					control.isSwiped(memberId);
				}
				catch (NumberFormatException e) {
					output("Invalid Member Id");
				}
				break;


				
			case RESTRICTED:
				input("Press <any key> to cancel");
				control.isCancel();
				break;
			
				
			case SCANNING:
				String bookStringInput = input("Scan Book (<enter> completes): "); //changed BoOk_StRiNg_InPuT to bookStringInput
				if (bookStringInput.length() == 0) {
					control.isComplete();
					break;
				}
				try {
					int BiD = Integer.valueOf(bookStringInput).intValue();
					control.isScanned(BiD);
					
				} catch (NumberFormatException e) {
					output("Invalid Book Id");
				} 
				break;

					
				
			case FINALISING:
				String ans = input("Commit loans? (Y/N): "); //changed AnS to ans
				if (ans.toUpperCase().equals("N")) {
					control.isCancel();
					
				} else {
					control.commitLoans();
					input("Press <any key> to complete ");
				}
				break;

				
				
			case COMPLETED:
				output("Borrowing Completed");
				return;
	
				
			default:
				output("Unhandled state");
				throw new RuntimeException("BorrowBookUI : unhandled state :" + state);			
			}
		}		
	}


	public void display(Object object) {
		output(object);		
	}




}
