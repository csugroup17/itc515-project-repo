package library.borrowbook;
import java.util.ArrayList;
import java.util.List;

import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;

public class BorrowBookControl {
	
	private BorrowBookUI uI;
	
	private Library library; //changed variable name lIbRaRy to library
	private Member member; //changed variable name mEmBeR to member
	private enum ControlState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED }; //changed enum CONTROL_STATE to ControlState
	private ControlState state; //changed variable sTaTe to state
	
	private List<Book> pendingList; //changed pEnDiNg_LiSt to PendingList
	private List<Loan> completedList; //changed cOmPlEtEd_LiSt to completedList
	private Book book; //changed variable name bOoK to book
	
	
	public BorrowBookControl() {
		this.library = Library.getInstance();
		state = ControlState.INITIALISED;
	}
	

	public void isSetUi(BorrowBookUI Ui) { //changed method name SeT_Ui to isSetUi
		if (!state.equals(ControlState.INITIALISED)) 
			throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
			
		this.uI = Ui;
		Ui.setState(BorrowBookUI.UiState.READY); 
		state = ControlState.READY;		
	}

		
	public void isSwiped(int memberId) {//changed method name SwIpEd to isSwiped and variable mEmBeR_Id to memberId
		if (!state.equals(ControlState.READY)) 
			throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
			
		member = library.getMember(memberId);
		if (member == null) {
			uI.display("Invalid memberId"); //changed method DiSpLaY to display
			return;
		}
		if (library.canMemberBorrow(member)) {
			pendingList = new ArrayList<>();
			uI.setState(BorrowBookUI.UiState.SCANNING);
			state = ControlState.SCANNING; 
		}
		else {
			uI.display("Member cannot borrow at this time");
			uI.setState(BorrowBookUI.UiState.RESTRICTED); 
		}
	}
	
	
	public void isScanned(int bookId) { //changed method name ScAnNeD to isScaned and variable name bOoKiD to bookId
		book = null;
		if (!state.equals(ControlState.SCANNING)) 
			throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
			
		book = library.getBook(bookId);
		if (book == null) {
			uI.display("Invalid bookId");
			return;
		}
		if (!book.isAvailable()) { //changed method name iS_AvAiLaBlE to isAvailable
			uI.display("Book cannot be borrowed");
			return;
		}
		pendingList.add(book);
		for (Book B : pendingList) 
			uI.display(B.toString());
		
		if (library.getNumberOfLoansRemainingForMember(member) - pendingList.size() == 0) {
			uI.display("Loan limit reached");
			isComplete(); //changed method name CoMpLeTe to isComplete
		}
	}
	
	
	public void isComplete() {
		if (pendingList.size() == 0) 
			isCancel();//changed CaNcEl to isCancel
		
		else {
			uI.display("\nFinal Borrowing List");
			for (Book book : pendingList) //changed bOoK to book
				uI.display(book.toString());
			
			completedList = new ArrayList<Loan>();
			uI.setState(BorrowBookUI.UiState.FINALISING);
			state = ControlState.FINALISING;
		}
	}


	public void commitLoans() { //changed method name CoMmIt_LoAnS to commitLoans
		if (!state.equals(ControlState.FINALISING)) 
			throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
			
		for (Book B : pendingList) {
			Loan loan = library.issueLoan(B, member); //changed lOaN to losn
			completedList.add(loan);			
		}
		uI.display("Completed Loan Slip");
		for (Loan loan : completedList) //changed LOAN to loan
			uI.display(loan.toString());
		
		uI.setState(BorrowBookUI.UiState.COMPLETED);
		state = ControlState.COMPLETED;
	}

	
	public void isCancel() {
		uI.setState(BorrowBookUI.UiState.CANCELLED);
		state = ControlState.CANCELLED;
	}
	
}
