
package main.java.com.mycompany.itc515_g17.library;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import library.borrowbook.BorrowBookUI;
import library.borrowbook.BorrowBookControl;
import library.entities.Book;
import library.entities.Calendar;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;
import library.fixbook.FixBookUi;
import library.fixbook.FixBookControl;
import library.payfine.PayFineUi;
import library.payfine.PayFineControl;
import library.returnBook.ReturnBookUI;
import library.returnBook.ReturnBookControl;

public class Main {


    private static Scanner IN;
    private static Library LIB;
    private static String MENU;
    private static Calendar CAL;
    private static SimpleDateFormat SDF;

    private static String getMenu() {
        /* Get_menu refactored to getMenu*/

        StringBuilder sb = new StringBuilder();

        sb.append("\nLibrary Main Menu\n\n")
                .append("  M  : add member\n")
                .append("  LM : list members\n")
                .append("\n")
                .append("  B  : add book\n")
                .append("  LB : list books\n")
                .append("  FB : fix books\n")
                .append("\n")
                .append("  L  : take out a loan\n")
                .append("  R  : return a loan\n")
                .append("  LL : list loans\n")
                .append("\n")
                .append("  P  : pay fine\n")
                .append("\n")
                .append("  T  : increment date\n")
                .append("  Q  : quit\n")
                .append("\n")
                .append("Choice : ");

        return sb.toString();
    }

    public static void main(String[] args) {
        try {
            IN = new Scanner(System.in);
            LIB = Library.getInstance();
            CAL = Calendar.getInstance();
            SDF = new SimpleDateFormat("dd/MM/yyyy");

            for (Member m : LIB.listMembers()) {
                /* lIsT_MeMbErS refactored to listMembers*/
                output(m);
            }
            output(" ");
            for (Book b : LIB.listBooks()) {
                /* lIsT_BoOkS refactored to listBooks*/
                output(b);
            }

            MENU = getMenu();

            boolean e = false;

            while (!e) {

                output("\n" + SDF.format(CAL.getDate()));
                String c = input(MENU);

                switch (c.toUpperCase()) {

                    case "M":
                        addMember();
                        /* ADD_MEMBER refactored to addMember*/
                        break;

                    case "LM":
                        listMembers();
                        /* LIST_MEMBERS refactored to listMembers*/
                        break;

                    case "B":
                        addBook();
                        /* ADD_BOOK refactored to addBook*/
                        break;

                    case "LB":
                        listBooks();
                        /* LIST_BOOKS refactored to listBooks*/
                        break;

                    case "FB":
                        fixBooks();
                        /* FIX_BOOKS refactored to fixBooks*/
                        break;

                    case "L":
                        borrowBook();
                        /* BORROW_BOOK refactored to borrowBook*/
                        break;

                    case "R":
                        returnBook();
                        /* RETURN_BOOK refactored to returnBook*/
                        break;

                    case "LL":
                        listCurrentLoans();
                        /* LIST_CURRENT_LOANS refactored to listCurrentLoans*/
                        break;

                    case "P":
                        payFines();
                        /* PAY_FINES refactored to payFines*/
                        break;

                    case "T":
                        incrementDate();
                        /* INCREMENT_DATE refactored to incrementDate*/
                        break;

                    case "Q":
                        e = true;
                        break;

                    default:
                        output("\nInvalid option\n");
                        break;
                }

                Library.save();
                /* Save refactored to save*/
            }
        } catch (RuntimeException e) {
            output(e);
        }
        output("\nEnded\n");
    }

    private static void payFines() {
        new PayFineUi(new PayFineControl()).run();
    }

    private static void listCurrentLoans() {
        output("");
        for (Loan loan : LIB.listCurrentLoans()) {
            /* lISt_CuRrEnT_LoAnS refactored to listCurrentLoans*/
            output(loan + "\n");
        }
    }

    private static void listBooks() {
        output("");
        for (Book book : LIB.listBooks()) {
            output(book + "\n");
        }
    }

    private static void listMembers() {
        output("");
        for (Member member : LIB.listMembers()) {
            output(member + "\n");
        }
    }

    private static void borrowBook() {

        new BorrowBookUI(new BorrowBookControl()).run(); //bORROW_bOOK_cONTROL refactored to borrowBookControl, Run to run	


    }

    private static void returnBook() {
        new ReturnBookUI(new ReturnBookControl()).run();
    }

    private static void fixBooks() {
        new FixBookUi(new FixBookControl()).run();
    }

    private static void incrementDate() {
        try {
            int days = Integer.valueOf(input("Enter number of days: ")).intValue();
            CAL.incrementDate(days);
            LIB.checkCurrentLoans();
            /* cHeCk_CuRrEnT_LoAnS refactored to checkCurrentLoans*/
            output(SDF.format(CAL.getDate()));

        } catch (NumberFormatException e) {
            output("\nInvalid number of days\n");
        }
    }

    private static void addBook() {

        String author = input("Enter author: ");
        /* AuThOr refactored to author*/
        String title = input("Enter title: ");
        /* TiTlE refactored to title*/
        String callNumber = input("Enter call number: ");
        /* CaLl_NuMbEr refactored to callNumber*/
        Book book = LIB.addBook(author, title, callNumber);
        /* Book refactored to book*/ /* aDd_BoOk refactored to addBook*/
        output("\n" + book + "\n");

    }

    private static void addMember() {
        try {
            String lastName = input("Enter last name: ");
            /* LaSt_NaMe refactored to lastName*/
            String firstName = input("Enter first name: ");
            /* FiRsT_NaMe refactored to firstName*/
            String emailAddress = input("Enter email address: ");
            /* EmAiL_AdDrEsS refactored to emailAddress*/
            int phoneNumber = Integer.valueOf(input("Enter phone number: ")).intValue();
            /* PhOnE_NuMbEr refactored to phoneNumber*/
            Member member = LIB.addMember(lastName, firstName, emailAddress, phoneNumber);
            /* MeMbEr refactored to member*/ /* aDd_MeMbEr refactored to addMember*/
            output("\n" + member + "\n");

        } catch (NumberFormatException e) {
            output("\nInvalid phone number\n");
        }

    }

    private static String input(String prompt) {
        System.out.print(prompt);
        return IN.nextLine();
    }

    private static void output(Object object) {
        System.out.println(object);
    }

}
